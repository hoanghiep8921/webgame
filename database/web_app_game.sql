-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: web_app
-- ------------------------------------------------------
-- Server version	5.7.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `game`
--

DROP TABLE IF EXISTS `game`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game` (
  `game_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `image` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appstore` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `playstore` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`game_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game`
--

LOCK TABLES `game` WRITE;
/*!40000 ALTER TABLE `game` DISABLE KEYS */;
INSERT INTO `game` VALUES (1,'Battle','https://genknews.genkcdn.vn/k:thumb_w/690/2015/8-1451470190066/top-20-hinh-nen-game-cho-may-tinh-dep-nhat-nam-2015.jpg','https://www.facebook.com/','https://www.google.com/','IOS','Fix very much bugs','vclvlclsdflslflsdfl'),(2,'API','http://1.bp.blogspot.com/-b0TGPPALvP4/VWNvjInFLzI/AAAAAAAAaKo/lYbh4vqkL6E/s1600/nhung-hinh-nen-game-dep-nhat-cho-may-tinh-10.jpg','https://www.facebook.com/','https://www.google.com/','Android','Fix very much bugs',NULL),(3,'Giết người','https://genknews.genkcdn.vn/k:thumb_w/690/2015/8-1451470190066/top-20-hinh-nen-game-cho-may-tinh-dep-nhat-nam-2015.jpg','https://www.facebook.com/','https://www.google.com/','IOS','Fix very much bugs',NULL),(4,'Hay','http://1.bp.blogspot.com/-b0TGPPALvP4/VWNvjInFLzI/AAAAAAAAaKo/lYbh4vqkL6E/s1600/nhung-hinh-nen-game-dep-nhat-cho-may-tinh-10.jpg','https://www.facebook.com/','https://www.google.com/','Android','Fix very much bugs',NULL),(5,'JAVA','https://genknews.genkcdn.vn/k:thumb_w/690/2015/8-1451470190066/top-20-hinh-nen-game-cho-may-tinh-dep-nhat-nam-2015.jpg','https://www.facebook.com/','https://www.google.com/','FaceBook','Fix very much bugs',NULL),(7,'vcl','uploads/1542186184-Capture.PNG','vcl','vcl','vcl','vcl','vcl'),(8,'vcl','uploads/1542186184-Capture.PNG',NULL,NULL,'vcl',NULL,NULL),(12,'vcl','uploads/1542187563-c41_522018 (1).jpg','vcl','dcm','vcl','dcm','dcm'),(13,'vcl','uploads/1542291039-hinh-nen-Batman-Arkham-Knight-Wallpaper-full-hd-08.jpg','vcl','vcl','vcl',NULL,'vcl');
/*!40000 ALTER TABLE `game` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-18 18:44:58
