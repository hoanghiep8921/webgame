(function($){
    //Cac thiet lap mac dinh
    var defaultOptions={};

    //Thiet lap plugin cho jquery
    $.fn.tabWeb= function(options){

        //trộn options người dùng truyền vào và defaulOptions
        var settings = $.extend({},defaultOptions,options);
        var self = $(this);

        //Thêm code xử lý
        function activeTab(tabItem){
            var $tabItem = $(tabItem);
            if($tabItem.hasClass('active')) return;
            if($tabItem.data('tab')){
                //show tab
                self.find('.tab-header-item.active').removeClass('active');
                $tabItem.addClass('active');

                //show content
                self.find('.tab-content-item.active').removeClass('active');
                $($tabItem.data('tab')).addClass('active')
            }
        }
        self.find('.tab-header-item').on('click',function(){
            activeTab(this);
            return false;
        });

        activeTab(self.find('.tab-header-item')[0]);

        return this;
    }

})(jQuery);