$(document).ready(function () {
    var dataProduct = {};
    var dataCategory = {};

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#preview-product-img').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#input-select-img-product").change(function () {
        readURL(this);
        var formData = new FormData();
        formData.append('file', $("#input-select-img-product")[0].files[0]);
        axios.post("/upload-image", formData).then(function (res) {
            if (res.data.success) {
                $('#preview-product-img').attr('src', res.data.link);
                imageUrl=res.data.link;
            }
        }, function (err) {
        })
    });

    $("#input-select-img-product-edit").change(function () {
        readURL(this);
        var formData = new FormData();
        formData.append('file', $("#input-select-img-product-edit")[0].files[0]);
        axios.post("/upload-image", formData).then(function (res) {
            if (res.data.success) {
                $('#preview-product-img-edit').attr('src', res.data.link);
                imageUrlEdit=res.data.link;
            }
        }, function (err) {
        })
    });

});