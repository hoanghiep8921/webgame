package com.example.demo.service;

import com.example.demo.model.Game;
import com.example.demo.repository.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.List;

@Service
public class GameService {
    @Autowired
    private GameRepository gameRepository;

    public List<Game> getListGame(){
        return gameRepository.findAll();
    }
    public List<Game> getByCategory(String category){
        return gameRepository.getByCategoryContains(category);
    }

    public Game findGameById(int gameId){
        return gameRepository.getOne(gameId);
    }

    public void addGame(Game game){
        gameRepository.save(game);
    }
    public void deleteGame(Game game){
        gameRepository.delete(game);
    }

    public List<Game> newGame(){
        return gameRepository.findAllByOrderByIdDesc();
    }



}
