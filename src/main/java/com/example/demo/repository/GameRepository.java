package com.example.demo.repository;

import com.example.demo.model.Game;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GameRepository extends JpaRepository<Game,Integer> {

    List<Game> getByCategoryContains(String category);
    List<Game> findAllByOrderByIdDesc();
}
