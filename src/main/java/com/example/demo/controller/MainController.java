package com.example.demo.controller;

import com.example.demo.model.DataApiResult;
import com.example.demo.model.FileUploadResult;
import com.example.demo.model.Game;
import com.example.demo.service.FileStorageService;
import com.example.demo.service.GameService;
import com.example.demo.status.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@Controller
public class MainController {
    @Autowired
    private GameService gameService;

    @Autowired
    FileStorageService storageService;

    @RequestMapping("/")
    public String  index(Model model){
        model.addAttribute("listGameIOS",gameService.getByCategory("IOS"));
        model.addAttribute("listGameAndroid",gameService.getByCategory("Android"));
        model.addAttribute("listGameFaceBook",gameService.getByCategory("Facebook"));
        List<Game> gameList=gameService.newGame();
        if(gameList.size()!=0){
            model.addAttribute( "newGame",gameList.get(0));
        }else{
            model.addAttribute( "newGame",new Game());
        }
        return "index";
    }

    @RequestMapping(value = "/findGame/{id}",method = RequestMethod.GET)
    @ResponseBody
    public DataApiResult findGame(@PathVariable int id){
        DataApiResult result = new DataApiResult();
        result.setData(gameService.findGameById(id));
        result.setMessage("Ok");
        result.setSuccess(true);
        return result;
    }
    @RequestMapping("/allGame")
    @ResponseBody
    public List<Game> getAllGame(){
        return gameService.getListGame();
    }

    @RequestMapping("/admin")
    public String  admin(Model model){
        model.addAttribute("listGame",gameService.getListGame());
        return "admin";
    }

    @RequestMapping(value = "/addGame",method = RequestMethod.POST)
    @ResponseBody
    public Result addGame(@RequestBody Game game){
        Result result= new Result();
        if(game.getImage()=="" && game.getName()==""&& game.getName()==""){
            result.setCode(400);
            result.setMessage("Fail");
            return result;
        }
        else {
            gameService.addGame(game);
            result.setCode(200);
            result.setMessage("Done");
            return result;
        }
    }

    @RequestMapping(value="/deleteGame/{id}",method = RequestMethod.POST)
    @ResponseBody
    public Result deleteGame(@PathVariable int id){
        Result result = new Result();
        Game game= gameService.findGameById(id);
        gameService.deleteGame(game);
        result.setCode(200);
        result.setMessage("Done");
        return result;
    }

    @RequestMapping(value = "/editGame/{id}",method = RequestMethod.POST)
    @ResponseBody
    public Result editGame(@PathVariable int id,@RequestBody Game game){
        Result result = new Result();
        Game gameCurrent= gameService.findGameById(id);
        gameCurrent.setImage(game.getImage());
        gameCurrent.setName(game.getName());
        gameCurrent.setCategory(game.getCategory());
        gameCurrent.setDescription(game.getDescription());
        gameCurrent.setAppstore(game.getAppstore());
        gameCurrent.setPlaystore(game.getPlaystore());
        gameCurrent.setFacebook(game.getFacebook());
        gameService.addGame(gameCurrent);
        result.setMessage("Done");
        result.setCode(200);
        return result;
    }



    @PostMapping("/upload-image")
    @ResponseBody
    public FileUploadResult uploadImage(
            @RequestParam("file") MultipartFile file) {
        String message = "";
        FileUploadResult result = new FileUploadResult();
        try {
            String newFilename = storageService.store(file);
            message = "You successfully uploaded " +
                    file.getOriginalFilename() + "!";
            result.setMessage(message);
            result.setSuccess(true);
            result.setLink("uploads/" +
                    newFilename);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage(e.getMessage());
        }
        return result;
    }

}
